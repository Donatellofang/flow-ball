﻿using UnityEngine;
using System.Collections;
using SightPlusInterface;

namespace SightPlusMiddleware
{
    public class EditorRemoteTextureBehaviour : AbstractRemoteTextureBehaviour
    {
        public Material Mat;
        public string TextureName;
        public string ImageId;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public override void LoadRemoteTexture(Material mat, string textureName, string imageId)
        {
            //
        }
    }
}
