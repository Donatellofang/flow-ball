﻿using UnityEngine;
using System.Collections;
using SightPlusInterface;

/// <summary>
/// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/// Make sure only use this once for one texture!!!
/// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/// </summary>
/// 

namespace SightPlusMiddleware
{
    public class RemoteTextureProxyV1 : MonoBehaviour
    {
        public Material Mat;
        public string TextureName;
        public string AndroidImageId;
        public string IOSImageId;

        // Use this for initialization
        void Start()
        {
#if UNITY_EDITOR
            Object prefab = Resources.Load("EditorRemoteTextureLoader");
            //Object prefab = Resources.Load("SelfDownloadRemoteTextureLoaderV1");
#else
            Object prefab = Resources.Load("SelfDownloadRemoteTextureLoaderV1");
#endif

            GameObject go = Instantiate(prefab) as GameObject;
            AbstractRemoteTextureBehaviour loader = go.GetComponent<AbstractRemoteTextureBehaviour>();

            loader.gameObject.transform.parent = gameObject.transform;

#if UNITY_EDITOR
            loader.LoadRemoteTexture(Mat, TextureName, IOSImageId);
#else
#if UNITY_IOS
        loader.LoadRemoteTexture(Mat, TextureName, IOSImageId);
#elif UNITY_ANDROID
        loader.LoadRemoteTexture(Mat, TextureName, AndroidImageId);
#endif
#endif

            loader.gameObject.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
