﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;

[CustomEditor(typeof(SightPlusMiddleware.RemoteVideoPlayerProxyV1))]
class RemoteVideoPlayerProxyV1Editor : Editor
{
    SightPlusMiddleware.RemoteVideoPlayerProxyV1 videoPlayer;

    private UploadPhase curPhase = UploadPhase.Waiting;

    enum UploadPhase
    {
        Waiting,
        ChooseVideo,
        Uploading,
    }

    private bool showVideoExistWarmming = false;

    private bool updateResult;

    void OnEnable()
    {
        videoPlayer = (SightPlusMiddleware.RemoteVideoPlayerProxyV1)target;
        showVideoExistWarmming = false;
    }

    public override void OnInspectorGUI()
    {
        videoPlayer.VideoWidgetId = EditorGUILayout.TextField("VideoWidgetId", videoPlayer.VideoWidgetId);
        if (videoPlayer.VideoWidgetId != "")
        {
            if (!videoPlayer.IsQuerying)
            {
                if (GUILayout.Button("Get Widget Info"))
                {
                    if (!CheckLockonState())
                    {
                        SightPlusMiddleware.RemoteVideoPlayerProxyV1.QueryWidgetInfo(videoPlayer);
                    }
                }
            }
            else
            {
                if (GUILayout.Button("Querying"))
                {
                }
            }
            if (videoPlayer.VideoResourceGroupId != "")
            {
                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField("--Widget Property--");
                EditorGUILayout.TextField("VideoResId", videoPlayer.VideoResourceGroupId);
                videoPlayer.VideoWidgetName = EditorGUILayout.TextField("VideoWidgetName", videoPlayer.VideoWidgetName);
                videoPlayer.Priority = EditorGUILayout.IntField("Priority", videoPlayer.Priority);
                videoPlayer.AutoScale = EditorGUILayout.Toggle("AutoScale", videoPlayer.AutoScale);
                videoPlayer.AutoPlay = EditorGUILayout.Toggle("AutoPlay", videoPlayer.AutoPlay);
                videoPlayer.LoopCount = EditorGUILayout.IntField("LoopCount", videoPlayer.LoopCount);
                videoPlayer.TransparentType = EditorGUILayout.IntField("TransparentType", videoPlayer.TransparentType);
                videoPlayer.Chroma = EditorGUILayout.ColorField("Chroma", videoPlayer.Chroma);
                videoPlayer.Thresh1 = EditorGUILayout.FloatField("Thresh1", videoPlayer.Thresh1);
                videoPlayer.Thresh2 = EditorGUILayout.FloatField("Thresh2", videoPlayer.Thresh2);
                videoPlayer.FullScreen = EditorGUILayout.Toggle("FullScreen", videoPlayer.FullScreen);
                videoPlayer.FullScreenDelay = EditorGUILayout.IntField("FullScreenDelay", videoPlayer.FullScreenDelay);
                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField("--GameObject Property--");
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Widget Parent");
                videoPlayer.VideoParent = EditorGUILayout.ObjectField(videoPlayer.VideoParent, typeof(Transform), true) as Transform;
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Callback FSM");
                videoPlayer.CallbackCom = EditorGUILayout.ObjectField(videoPlayer.CallbackCom, typeof(Component), true) as Component;
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.LabelField("");
            }
        }

        EditorGUILayout.BeginHorizontal();
        if (videoPlayer.VideoWidgetId != "" && videoPlayer.VideoResourceGroupId != "")
        {
            if (!videoPlayer.IsUpdating)
            {
                if (GUILayout.Button("Update"))
                {
                    if (!CheckLockonState())
                    {
                        SightPlusMiddleware.RemoteVideoPlayerProxyV1.UpdateWidget(videoPlayer);
                    }
                }
            }
            else
            {
                if (GUILayout.Button("Updating..."))
                {
                }
            }
            GUILayout.Space(10f);
        }
        if (GUILayout.Button("New Video"))
        {
            if (curPhase == UploadPhase.Waiting)
            {
                curPhase = UploadPhase.ChooseVideo;
                showVideoExistWarmming = false;
            }
            else if (curPhase == UploadPhase.ChooseVideo)
            {
                curPhase = UploadPhase.Waiting;
            }
        }
        EditorGUILayout.EndHorizontal();

        switch (curPhase)
        {
            case UploadPhase.Waiting:
                break;
            case UploadPhase.ChooseVideo:
                videoPlayer.VideoPath = EditorGUILayout.TextField("VideoPath", videoPlayer.VideoPath);
                if (showVideoExistWarmming)
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.LabelField("Video file doesn't exist..");
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Create New Widget"))
                {
                    if (!CheckLockonState())
                    {
                        if (File.Exists(videoPlayer.VideoPath))
                        {
                            SightPlusMiddleware.RemoteVideoPlayerProxyV1.StartUpload(videoPlayer, true);
                            curPhase = UploadPhase.Uploading;
                            showVideoExistWarmming = false;
                        }
                        else
                        {
                            showVideoExistWarmming = true;
                        }
                    }
                }
                if (videoPlayer.VideoWidgetId != "")
                {
                    GUILayout.Space(10f);
                    if (GUILayout.Button("Update this Widget"))
                    {
                        if (!CheckLockonState())
                        {
                            if (File.Exists(videoPlayer.VideoPath))
                            {
                                SightPlusMiddleware.RemoteVideoPlayerProxyV1.StartUpload(videoPlayer, false);
                                curPhase = UploadPhase.Uploading;
                                showVideoExistWarmming = false;
                            }
                            else
                            {
                                showVideoExistWarmming = true;
                            }
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
                break;
            case UploadPhase.Uploading:
                EditorGUILayout.TextField("VideoPath", videoPlayer.VideoPath);
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                EditorGUILayout.LabelField("Uploading..." + videoPlayer.UploadProgress + "%");
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();
                if (!videoPlayer.IsUploading)
                {
                    curPhase = UploadPhase.Waiting;
                    Repaint();
                }
                break;
            default:
                break;
        }

        if (CheckLockonState())
        {
            EditorUtility.SetDirty(target);
        }
    }

    bool CheckLockonState()
    {
        return (videoPlayer.IsUploading || videoPlayer.IsUpdating || videoPlayer.IsQuerying);
    }
}
