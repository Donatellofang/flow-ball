﻿using UnityEngine;
using System.Collections;

public class BasketBallCtrl : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        m_TopScore = PlayerPrefs.GetInt("SightPlusBasketballTopScore", 0);
        topScoreUI.NumberValue = m_TopScore;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private int m_Score;
    private int m_TopScore;
    private float m_GameTimer;
    private bool m_IsPlaying = false;
    private const float One_Game_Time = 90f;
    private GameObject m_LastBall;

    public RealTimeUI timeUI;
    public RealNumberUI curScoreUI;
    public RealNumberUI topScoreUI;

    public void BallIn(float distance, GameObject ball)
    {
        if (!m_IsPlaying)
        {
            Reset();
            StartCoroutine(OnPlaying());
        }
        else if (ball != m_LastBall)
        {
            m_LastBall = ball;

            int scoreValue = -1;
            if (distance > 250f)
            {
                scoreValue = 3;
            }
            else if (distance > 120f)
            {
                scoreValue = 2;
            }
            m_Score = Mathf.Clamp(m_Score + scoreValue, 0, 999);
            curScoreUI.NumberValue = m_Score;
            curScoreUI.GetComponent<PlayMakerFSM>().SendEvent("Shake");
        }
    }

    void OnEnable()
    {
        if (m_IsPlaying)
        {
            StartCoroutine(OnPlaying());
        }
    }

    void Reset()
    {
        m_GameTimer = One_Game_Time;
        m_Score = 0;
        m_IsPlaying = false;
    }

    IEnumerator OnPlaying()
    {
        m_IsPlaying = true;

        timeUI.GetComponent<PlayMakerFSM>().SendEvent("Shake");

        while (m_IsPlaying)
        {
            m_GameTimer -= Time.deltaTime;
            if (m_GameTimer <= 0f)
            {
                if (m_Score > m_TopScore)
                {
                    m_TopScore = m_Score;
                    topScoreUI.NumberValue = m_TopScore;
                    PlayerPrefs.SetInt("SightPlusBasketballTopScore", m_TopScore);

                    topScoreUI.GetComponent<PlayMakerFSM>().SendEvent("Shake");
                }

                m_GameTimer = 0f;
                m_Score = 0;
                curScoreUI.NumberValue = 0;
                m_IsPlaying = false;

                timeUI.TimeValue = (int)m_GameTimer;
            }
            else
            {
                timeUI.TimeValue = (int)m_GameTimer + 1;
            }

            yield return new WaitForEndOfFrame();
        }

        timeUI.GetComponent<PlayMakerFSM>().SendEvent("Shake");
    }
}
