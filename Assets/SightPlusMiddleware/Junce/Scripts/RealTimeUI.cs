﻿using UnityEngine;
using System.Collections;

public class RealTimeUI : MonoBehaviour
{

    public int TimeValue { get; set; }
    private int m_RecTime;

    public Transform[] minute10;
    public Transform[] minute1;
    public Transform[] second10;
    public Transform[] second1;

	// Use this for initialization
	void Start () {
        m_RecTime = TimeValue;
	}
	
	// Update is called once per frame
	void Update () {
	    if (m_RecTime != TimeValue)
	    {
            m_RecTime = TimeValue;
            ChangeTime();
	    }
	    
	}

    void ChangeTime()
    {
        int mm = TimeValue / 60;
        int ss = TimeValue % 60;

        int temp = mm % 100 / 10;
        for (int i = 0; i < minute10.Length; i++)
        {
            if (i == temp)
            {
                minute10[i].gameObject.SetActive(true);
            } 
            else
            {
                minute10[i].gameObject.SetActive(false);
            }
        }
        temp = mm % 10;
        for (int i = 0; i < minute1.Length; i++)
        {
            if (i == temp)
            {
                minute1[i].gameObject.SetActive(true);
            }
            else
            {
                minute1[i].gameObject.SetActive(false);
            }
        }
        temp = ss % 100 / 10;
        for (int i = 0; i < second10.Length; i++)
        {
            if (i == temp)
            {
                second10[i].gameObject.SetActive(true);
            }
            else
            {
                second10[i].gameObject.SetActive(false);
            }
        }
        temp = ss % 10;
        for (int i = 0; i < second1.Length; i++)
        {
            if (i == temp)
            {
                second1[i].gameObject.SetActive(true);
            }
            else
            {
                second1[i].gameObject.SetActive(false);
            }
        }
    }
}
