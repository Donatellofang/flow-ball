﻿using UnityEngine;
using System.Collections;

public class ScreenBallCtrl : MonoBehaviour
{

    public Transform ball;

    public Animator anim;

    // Use this for initialization
    void Start()
    {

    }

    private bool m_HasPress = false;
    private Vector3 m_StartPoint;

    //public UISlider powerSlider;

    // Update is called once per frame
    void Update()
    {
        transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0f, 60f));

        if (!m_HasPress)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                m_HasPress = true;
                m_StartPoint = Input.mousePosition;
            }
#else
            if (Input.touchCount != 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    m_HasPress = true;
                    m_StartPoint = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0f);
                }
            }
#endif
        }
        else
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(0))
            {
                m_HasPress = false;

                float power = Mathf.Clamp01((Input.mousePosition.y - m_StartPoint.y) * 2f / Screen.height);
                Transform newBall = Instantiate(ball, ball.position, ball.rotation) as Transform;
                newBall.parent = transform;
                newBall.localScale = ball.localScale;
                newBall.parent = null;
                newBall.gameObject.SetActive(true);
                newBall.rigidbody.velocity = Camera.main.transform.forward * 120f * power + Camera.main.transform.forward * 20f + Camera.main.transform.up * 175f * power + Camera.main.transform.up * 35f;
                newBall.rigidbody.angularVelocity = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));

                anim.SetTrigger("Shoot");
            }
#else
            if (Input.touchCount != 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    float power = Mathf.Clamp01((Input.GetTouch(0).position.y - m_StartPoint.y) * 2f / Screen.height);
                }
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    m_HasPress = false;

                    float power = Mathf.Clamp01((Input.GetTouch(0).position.y - m_StartPoint.y) * 2f / Screen.height);
                    Transform newBall = Instantiate(ball, ball.position, ball.rotation) as Transform;
                    newBall.parent = transform;
                    newBall.localScale = ball.localScale;
                    newBall.parent = null;
                    newBall.gameObject.SetActive(true);
                    newBall.rigidbody.velocity = Camera.main.transform.forward * 120f * power + Camera.main.transform.forward * 20f + Camera.main.transform.up * 175f * power + Camera.main.transform.up * 35f;
                    newBall.rigidbody.angularVelocity = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));

                    anim.SetTrigger("Shoot");
                }
            }
#endif
        }
    }
}
