﻿using UnityEngine;
using System.Collections;
using SightPlusInterface;

namespace SightPlusMiddleware
{
    public class DummyCustomUIBehaviour : AbstractCustomUIBehaviour
    {

        public override void HideUI()
        {
            Debug.Log("Hide native ui.");
        }

        public override void ShowUI()
        {
            Debug.Log("Show native ui.");
        }
    } 
}
