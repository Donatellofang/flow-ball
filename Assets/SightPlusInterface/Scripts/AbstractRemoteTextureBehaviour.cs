﻿using UnityEngine;
using System.Collections;

namespace SightPlusInterface
{
    public class AbstractRemoteTextureBehaviour : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public virtual void LoadRemoteTexture(Material mat, string textureName, string imageId)
        {
            return;
        }
    }
}
