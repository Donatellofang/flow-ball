﻿using UnityEngine;
using System.Collections;

namespace SightPlusInterface
{
    public class AbstractCustomUIBehaviour : MonoBehaviour
    {
        public virtual void HideUI()
        {

        }

        public virtual void ShowUI()
        {

        }
    } 
}
