﻿using UnityEngine;
using System.Collections;

namespace SightPlusInterface
{
    public class AbstractRemoteVideoPlayerBehaviour : MonoBehaviour
    {
        public virtual void SetVideoWidget(string id, GameObject videoParent, System.Action callback)
        {
            return;
        }

        public virtual void SetAutoPlay(bool auto)
        {
            return;
        }

        public virtual void SetLoopCount(int loop)
        {
            return;
        }

        public virtual bool SetVolume(float value)
        {
            return true;
        }

        public virtual bool SetFullscreen(bool fullscreen)
        {
            return true;
        }

        public virtual int GetVideoWidth()
        {
            return 0;
        }

        public virtual int GetVideoHeight()
        {
            return 0;
        }

        public virtual float GetVideoLength()
        {
            return 0;
        }
        public virtual float GetCurrentPosition()
        {
            return 0;
        }

        public virtual bool IsPlaying()
        {
            return false;
        }

        public virtual bool IsEnd()
        {
            return false;
        }

        public virtual bool IsDownloaded()
        {
            return false;
        }

        public virtual bool Play()
        {
            return false;
        }

        public virtual bool Restart()
        {
            return false;
        }

        public virtual bool Pause()
        {
            return false;
        }

        public virtual bool Stop()
        {
            return false;
        }

        public virtual bool SeekTo(float position)
        {
            return true;
        }

        public virtual int GetDownloadProgress()
        {
            return 0;
        }
    }
}
