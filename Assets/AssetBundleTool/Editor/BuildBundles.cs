﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Security.Cryptography;

namespace EasyARBundleTool
{
    public class BuildBundles : MonoBehaviour
    {
        private const string Crpyto_Bundle_Extend = ".easyarbundle";

        [MenuItem("EasyARBundle Tools/Load Bundle(Only in Play mode)")]
        static void LoadBundle()
        {
            Object[] SelectedBundle = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
            foreach (var item in SelectedBundle)
            {
                string bundlePath = AssetDatabase.GetAssetPath(item);
                AsyncLoadEasyARBundle.LoadAsset("file://" + Application.dataPath.Replace(@"/Assets", @"/") + bundlePath, (assetBundle) => { Instantiate(assetBundle.mainAsset); });
            }
        }

        //[MenuItem("EasyARBundle Tools/Create AssetBundle - All Platform")]
        public static void CreateAssetBundlesAllPlatform()
        {

            //获取在Project视图中选择的所有游戏对象
            Object[] selectedAssets = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

            if (!Directory.Exists(Application.dataPath + "/StreamingAssets/"))
            {
                AssetDatabase.CreateFolder("Assets", "StreamingAssets");
            }

            BuildBundleAll(selectedAssets);

            //刷新编辑器
            AssetDatabase.Refresh();
        }

        //[MenuItem("EasyARBundle Tools/Create AssetBundle - Standalone")]
        public static string[] CreateAssetBundles_Standalone()
        {
            //获取在Project视图中选择的所有游戏对象
            Object[] SelectedAssets = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

            if (!Directory.Exists(Application.dataPath + "/StreamingAssets/"))
            {
                AssetDatabase.CreateFolder("Assets", "StreamingAssets");
            }

            string []bundlesPath = BuildBundle(SelectedAssets, BuildTarget.StandaloneWindows);

            //刷新编辑器
            AssetDatabase.Refresh();

            return bundlesPath;
        }

        //[MenuItem("EasyARBundle Tools/Create AssetBundle - IOS")]
        public static string[] CreateAssetBundles_IOS()
        {
            //获取在Project视图中选择的所有游戏对象
            Object[] SelectedAssets = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

            if (!Directory.Exists(Application.dataPath + "/StreamingAssets/"))
            {
                AssetDatabase.CreateFolder("Assets", "StreamingAssets");
            }

            string []bundlesPath = BuildBundle(SelectedAssets, BuildTarget.iPhone);

            //刷新编辑器
            AssetDatabase.Refresh();

            return bundlesPath;
        }

        //[MenuItem("EasyARBundle Tools/Create AssetBundle - Android")]
        public static string[] CreateAssetBundles_Android()
        {
            //获取在Project视图中选择的所有游戏对象
            Object[] SelectedAssets = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

            if (!Directory.Exists(Application.dataPath + "/StreamingAssets/"))
            {
                AssetDatabase.CreateFolder("Assets", "StreamingAssets");
            }

            string []bundlesPath = BuildBundle(SelectedAssets, BuildTarget.Android);

            //刷新编辑器
            AssetDatabase.Refresh();

            return bundlesPath;
        }

        //[MenuItem("EasyARBundle Tools/Create AssetBundle - WebPlayer")]
        public static string[] CreateAssetBundles_WebPlayer()
        {
            //获取在Project视图中选择的所有游戏对象
            Object[] SelectedAssets = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

            if (!Directory.Exists(Application.dataPath + "/StreamingAssets/"))
            {
                AssetDatabase.CreateFolder("Assets", "StreamingAssets");
            }

            string []bundlesPath = BuildBundle(SelectedAssets, BuildTarget.WebPlayer);

            //刷新编辑器
            AssetDatabase.Refresh();

            return bundlesPath;
        }

        public static string[] BuildBundle(Object[] selectedAssets, BuildTarget buildTarget)
        {
            BundleVersion bundleVer = GetCurrentBundleVersion();

            string[] bundlesPath = new string[selectedAssets.Length];
            for (int i = 0; i < selectedAssets.Length; i++ )
            {
                Object obj = selectedAssets[i];
                if (!ValidSelection(obj as GameObject))
                {
                    continue;
                }

                string sourcePath = AssetDatabase.GetAssetPath(obj);
                Debug.Log("Source Path : " + sourcePath);
                string objName = obj.name;
                Debug.Log("objName : " + objName);
                AssetDatabase.RenameAsset(sourcePath, "root");

                if (!Directory.Exists(Application.dataPath + "/StreamingAssets/" + objName.Replace(".prefab", "")))
                {
                    AssetDatabase.CreateFolder("Assets/StreamingAssets", objName.Replace(".prefab", ""));
                }

                string targetPath = Application.dataPath + "/StreamingAssets/" + objName.Replace(".prefab", "") + "/" + objName.Replace(".prefab", "") + "." + bundleVer + "." + buildTarget + ".assetbundle";
                if (BuildPipeline.BuildAssetBundle(obj, null, targetPath, BuildAssetBundleOptions.CollectDependencies, buildTarget))
                {
                    bundlesPath[i] = EncryptAssetBundle(targetPath, objName, buildTarget);
                    Debug.Log(objName + " : " + buildTarget + " 资源打包成功");
#if KeepSourceBundle
                
#else
                    AssetDatabase.Refresh();
                    if (File.Exists(targetPath))
                    {
                        AssetDatabase.DeleteAsset("Assets/StreamingAssets/" + objName.Replace(".prefab", "") + "/" + objName.Replace(".prefab", "") + "." + bundleVer + "." + buildTarget + ".assetbundle");
                    }
#endif
                }
                else
                {
                    Debug.Log(objName + " : " + buildTarget + " 资源打包失败");
                }

                AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(obj), objName);
            }

            return bundlesPath;
        }

        public static void BuildBundleAll(Object[] selectedAssets)
        {
            BuildBundle(selectedAssets, BuildTarget.StandaloneWindows);
            BuildBundle(selectedAssets, BuildTarget.iPhone);
            BuildBundle(selectedAssets, BuildTarget.Android);
            BuildBundle(selectedAssets, BuildTarget.WebPlayer);
        }

        public static string EncryptAssetBundle(string targetPath, string objName, BuildTarget buildTarget)
        {
            string newBundlePath = null;
            try
            {
                FileStream fs = new FileStream(targetPath, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);

                BundleVersion bundleVer = GetCurrentBundleVersion();

                byte[] encryptBundle = BundleCryptography.PackAssetBundle(br.ReadBytes((int)fs.Length), bundleVer);
                br.Close();
                fs.Close();
                newBundlePath = Application.dataPath + "/StreamingAssets/" + objName.Replace(".prefab", "") + "/" + objName.Replace(".prefab", "") + "." + bundleVer + "." + buildTarget + Crpyto_Bundle_Extend; ;
                fs = new FileStream(newBundlePath, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(encryptBundle);
                bw.Close();
                fs.Close();
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.ToString());
            }

            return newBundlePath;
        }

        public static BundleVersion GetCurrentBundleVersion()
        {
#if   UNITY_4_5
            return BundleVersion.Ver2_0_0;
#elif UNITY_4_6
            return BundleVersion.Ver2_1_1;
#else
            // Fail loudly.
            Debug.LogError("不支持当前Unity版本！");
            EditorUtility.DisplayDialog("Unity版本错误", "不支持当前Unity版本！", "确定");
#endif
        }

        /// <summary>
        /// 返回unity版本号
        /// 只返回主版本和小版本号，如Unity4.3
        /// </summary>
        /// <returns>unity版本号</returns>
        public static string GetUnityVersion()
        {
            string version = Application.unityVersion;

            char[] seperator = ".".ToCharArray();
            string[] substrings = version.Split(seperator, System.StringSplitOptions.RemoveEmptyEntries);

            if (substrings.Length > 1)
            {
                return substrings[0] + "." + substrings[1];
            }
            else if (substrings.Length == 1)
            {
                return substrings[0];
            }

            return version;
        }

        static bool ValidSelection(GameObject selectedObject)
        {
            if (selectedObject == null)
            {
                Debug.Log("Selection Error. Not a game object");
                EditorUtility.DisplayDialog("选择错误", "选择导出的资源必须是Prefab", "确定");
                return false;
            }
            if (!EditorUtility.IsPersistent(selectedObject))
            {
                EditorUtility.DisplayDialog("选择错误", "选择导出的资源必须在工作目录里面。场景中的物体不允许导出。", "确定");
                Debug.Log("Please Select a prefab from your project tree, Scene objects cannot be used for asset bundles");
                return false;
            }

            // TODO:: check root behaviour!!!

            return true;
        }
    }
}
