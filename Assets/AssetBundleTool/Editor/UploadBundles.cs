﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace EasyARBundleTool
{
    public class UploadBundles : MonoBehaviour
    {
        public static void Upload(BuildWindow.BuildSettingArgs setting, string bundlePath)
        {
            BundleUploader.UpdateBundle(bundlePath, "30", setting.ieId, setting.bundleVersion.ToString(), setting.buildTarget.ToString());
        }
    }
}
