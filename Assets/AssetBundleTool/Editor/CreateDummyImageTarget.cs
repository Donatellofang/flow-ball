﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace EasyARBundleTool
{
    public class CreateDummyImageTarget : MonoBehaviour
    {
        [MenuItem("EasyARBundle Tools/Create Dummy Image Target")]
        static void CreateImgPlane()
        {
            Object[] images = Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
            if (images.Length > 0)
            {
                Texture2D image = images[0] as Texture2D;
                Object obj = AssetDatabase.LoadAssetAtPath("Assets/AssetBundleTool/Prefabs/DummyImageTarget.prefab", typeof(Object));
                if (!obj)
                {
                    Debug.LogError("Load AssetBundleTool/Prefabs/DummyImageTarget.prefab failed");
                    return;
                }
                GameObject plane = Instantiate(obj, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                plane.transform.localScale = new Vector3(100, 100, 100);
                plane.name = "DummyImageTarget";
                DummyImageTarget dummyPlaneTarget = plane.GetComponent<DummyImageTarget>();
                if (!dummyPlaneTarget)
                {
                    Debug.LogError("plane.GetComponent<DummyImageTarget> failed");
                    return;
                }

                ChangeTextureImporter(image);
                dummyPlaneTarget.SetImage(image);
            }
        }

        //修改texture为Editor GUI and Legacy GUI，以保留纹理的原始尺寸
        static void ChangeTextureImporter(Texture2D texture)
        {
            string path = AssetDatabase.GetAssetPath(texture);
            TextureImporter texImporter = GetTextureSettings(path);
            TextureImporterSettings tis = new TextureImporterSettings();
            texImporter.ReadTextureSettings(tis);
            tis.ApplyTextureType(TextureImporterType.GUI, false);
            texImporter.SetTextureSettings(tis);
            AssetDatabase.ImportAsset(path);
        }

        static TextureImporter GetTextureSettings(string path)
        {
            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
            textureImporter.textureType = TextureImporterType.GUI;
            textureImporter.npotScale = TextureImporterNPOTScale.ToNearest;
            return textureImporter;
        }
    }
}
