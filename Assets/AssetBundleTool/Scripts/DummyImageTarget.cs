﻿using UnityEngine;
using System.Collections;

namespace EasyARBundleTool
{
    [ExecuteInEditMode]
    public class DummyImageTarget : MonoBehaviour
    {
        private Texture2D image = null;
        private int meshXNum = 1;
        private int meshYNum = 1;

        private Material mat;
        private Vector3[] vertices;
        private Vector2[] uvs;
        private int[] triangles;
        //private float previousImageWidth;
        //private float previousImageHeight;
        private float imageWidth;
        private float imageHeight;
        private float previousScaleX;
        //private int previousMeshXNum;
        void Awake()
        {
            mat = new Material(Shader.Find("Transparent/Diffuse"));

        }

        // 根据scale.x系数来等比缩放
        void Update()
        {
            float scaleX = transform.localScale.x;
            if (scaleX != previousScaleX)
            {
                previousScaleX = scaleX;
                Vector3 newScale = new Vector3(scaleX, scaleX, scaleX);
                transform.localScale = newScale;
            }

        }

        //创建 meshXNum*meshYNum 的网格
        private Mesh CreateMash()
        {
            Mesh mesh = new Mesh();
            InitVertexPos();
            InitUV();
            InitTriangles();
            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
            return mesh;
        }

        //设置网格顶点位置
        private void InitVertexPos()
        {
            int currentIndex = 0;
            vertices = new Vector3[(meshXNum + 1) * (meshYNum + 1)];

            for (int i = 0; i <= meshYNum; i++)
            {
                for (int j = 0; j <= meshXNum; j++)
                {
                    vertices[currentIndex] = new Vector3(j, 0, (float)(i * imageHeight / imageWidth));
                    currentIndex++;
                }
            }

            Vector3 offset = new Vector3((float)meshXNum / 2, 0, (float)(meshYNum * imageHeight / imageWidth / 2));
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3 tmp = vertices[i] - offset;
                vertices[i] = tmp;
            }
        }

        //设置网格UV坐标
        private void InitUV()
        {
            uvs = new Vector2[vertices.Length];
            for (int i = 0; i < uvs.Length; i++)
            {
                int x = i % (meshXNum + 1);
                int y = i / (meshXNum + 1);
                uvs[i] = new Vector2((float)x / meshXNum, (float)y / meshYNum);
            }
        }

        //设置网格三角面
        private void InitTriangles()
        {
            int currentIndex = 0;
            triangles = new int[meshXNum * meshYNum * 6];

            for (int i = 0; i < meshYNum; i++)
            {
                for (int j = 0; j < meshXNum; j++)
                {
                    int tmp = (meshXNum + 1) * i + j;
                    triangles[currentIndex++] = tmp;
                    triangles[currentIndex++] = tmp + meshXNum + 1;
                    triangles[currentIndex++] = tmp + meshXNum + 2;

                    triangles[currentIndex++] = tmp;
                    triangles[currentIndex++] = tmp + meshXNum + 2;
                    triangles[currentIndex++] = tmp + 1;

                }
            }

        }

        public void SetImage(Texture2D tmp)
        {
            this.image = tmp;
            if (image != null)
            {
                imageWidth = image.width;
                imageHeight = image.height;
            }
            //previousMeshXNum = meshYNum = meshXNum;
            GetComponent<MeshFilter>().mesh = CreateMash();
            //previousImageWidth = imageWidth;
            //previousImageHeight = imageHeight;

            mat.mainTexture = image;
            GetComponent<Renderer>().material = mat;
        }
    }
}
