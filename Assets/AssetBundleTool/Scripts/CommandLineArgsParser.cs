﻿using UnityEngine;
using System.Collections;

namespace EasyARBundleTool
{
    public class CommandLineArgsParser
    {

        string argPrefix = "+";

        public CommandLineArgsParser(string prefix)
        {
            argPrefix = prefix;
        }

        public void PrintAllArgs()
        {
            string[] args = GetAllArgs();
            foreach (string arg in args)
            {
                Debug.Log(arg);
            }
        }

        public string[] GetAllArgs()
        {
            return System.Environment.GetCommandLineArgs();
        }

        public bool HasArg(string argName)
        {
            return System.Environment.CommandLine.Contains(argPrefix + argName);
        }

        public int FindArg(string argName)
        {
            string[] args = GetAllArgs();
            argName = argPrefix + argName;

            int pos = -1;
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].ToLower().Equals(argName.ToLower()))
                {
                    pos = i;
                }
            }

            return pos;
        }

        public string GetArgAt(int i)
        {
            string[] args = GetAllArgs();

            if (args.Length <= i) return null;
            else return args[i];
        }

        public bool GetArgString(string argName, out string argVal)
        {
            argVal = "";
            int pos = FindArg(argName);
            if (pos < 0)
            {
                return false;
            }

            string _argVal = GetArgAt(pos + 1);

            if (_argVal == null)
            {
                return false;
            }
            else
            {
                argVal = _argVal;
                return true;
            }
        }

        public bool GetArgBool(string argName)
        {
            int pos = FindArg(argName);
            if (pos < 0) return false;
            else return true;
        }

        public bool GetArgInt(string argName, out int argVal)
        {
            argVal = 0;
            int pos = FindArg(argName);
            if (pos < 0)
            {
                return false;
            }

            string _argVal = GetArgAt(pos + 1);

            if (_argVal == null)
            {
                return false;
            }

            bool parseResult = true;
            try
            {
                int.TryParse(_argVal, out argVal);
            }
            catch
            {
                parseResult = false;
            }

            return parseResult;
        }

        public bool GetArgFloat(string argName, out float argVal)
        {
            argVal = 0;
            int pos = FindArg(argName);
            if (pos < 0)
            {
                return false;
            }

            string _argVal = GetArgAt(pos + 1);

            if (_argVal == null)
            {
                return false;
            }

            bool parseResult = true;
            try
            {
                float.TryParse(_argVal, out argVal);
            }
            catch
            {
                parseResult = false;
            }

            return parseResult;
        }
    }
}
