﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System;

namespace EasyARBundleTool
{
    public class DataCryptography
    {

        public static byte[] TripleDESEncryptData(byte[] data, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (data == null || data.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an TripleDESCryptoServiceProvider object
            // with the specified key and IV.
            using (TripleDESCryptoServiceProvider tdsAlg = new TripleDESCryptoServiceProvider())
            {
                tdsAlg.Key = Key;
                tdsAlg.IV = IV;

                tdsAlg.Mode = CipherMode.ECB;
                tdsAlg.Padding = PaddingMode.None;

                int fixedSize = (data.Length / 8 + 1) * 8;
                byte[] fixedData = new byte[fixedSize];
                data.CopyTo(fixedData, 0);
                for (int index = data.Length; index < fixedSize; index++)
                {
                    fixedData[index] = (byte)(fixedSize - data.Length);
                }

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = tdsAlg.CreateEncryptor(tdsAlg.Key, tdsAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        Debug.Log("Data length: " + data.Length);
                        csEncrypt.Write(fixedData, 0, fixedData.Length);
                        csEncrypt.FlushFinalBlock();
                        encrypted = msEncrypt.ToArray();
                        Debug.Log("Encrypted length: " + encrypted.Length);
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        public static byte[] TripleDESDecryptData(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold
            // the decrypted text.
            byte[] plaintext = null;

            // Create an TripleDESCryptoServiceProvider object
            // with the specified key and IV.
            using (TripleDESCryptoServiceProvider tdsAlg = new TripleDESCryptoServiceProvider())
            {
                tdsAlg.Key = Key;
                tdsAlg.IV = IV;

                tdsAlg.Mode = CipherMode.ECB;
                tdsAlg.Padding = PaddingMode.None;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = tdsAlg.CreateDecryptor(tdsAlg.Key, tdsAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        Debug.Log("CipherText length: " + cipherText.Length);
                        plaintext = new byte[msDecrypt.Length];
                        csDecrypt.Read(plaintext, 0, (int)msDecrypt.Length);

                        int flag = plaintext[plaintext.Length - 1];

                        int length = plaintext.Length - flag;
                        byte[] fixedPlainText = new byte[length];
                        Buffer.BlockCopy(plaintext, 0, fixedPlainText, 0, length);

                        plaintext = fixedPlainText;

                        Debug.Log("PlainText length: " + plaintext.Length);
                    }
                }

            }
            return plaintext;

        }

        public static byte[] NaiveBitEncryptData(byte[] src, byte[] key)
        {
            byte[] dst = new byte[src.Length];

            int i = 0;
            for (i = 0; i < src.Length - 8; i += 8)
            {
                dst[i] = (byte)((~src[i]) ^ key[0]);
                dst[i + 1] = (byte)((~src[i + 1]) ^ key[1]);
                dst[i + 2] = (byte)((~src[i + 2]) ^ key[2]);
                dst[i + 3] = (byte)((~src[i + 3]) ^ key[3]);
                dst[i + 4] = (byte)((~src[i + 4]) ^ key[4]);
                dst[i + 5] = (byte)((~src[i + 5]) ^ key[5]);
                dst[i + 6] = (byte)((~src[i + 6]) ^ key[6]);
                dst[i + 7] = (byte)((~src[i + 7]) ^ key[7]);
            }


            for (; i < src.Length; i++)
            {
                dst[i] = (byte)((~src[i]) ^ key[i % 8]);
            }

            return dst;
        }

        public static byte[] NaiveBitDecryptData(byte[] src, byte[] key)
        {
            byte[] dst = new byte[src.Length];

            int i = 0;
            for (i = 0; i < src.Length - 8; i += 8)
            {
                dst[i] = (byte)(~(src[i] ^ key[0]));
                dst[i + 1] = (byte)(~(src[i + 1] ^ key[1]));
                dst[i + 2] = (byte)(~(src[i + 2] ^ key[2]));
                dst[i + 3] = (byte)(~(src[i + 3] ^ key[3]));
                dst[i + 4] = (byte)(~(src[i + 4] ^ key[4]));
                dst[i + 5] = (byte)(~(src[i + 5] ^ key[5]));
                dst[i + 6] = (byte)(~(src[i + 6] ^ key[6]));
                dst[i + 7] = (byte)(~(src[i + 7] ^ key[7]));
            }


            for (; i < src.Length; i++)
            {
                dst[i] = (byte)(~(src[i] ^ key[i % 8]));
            }

            return dst;
        }
    }
}
