﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace EasyARBundleTool
{
    public class BundleUploader : MonoBehaviour
    {
        private const string Crpyto_Bundle_Extend = ".easyarbundle";

        public float UploadProgress { get; private set; }
        public bool IsUploading { get; private set; }

        private static BundleUploader m_Instance = null;
        public static BundleUploader Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<BundleUploader>();
                    if (m_Instance != null)
                    {
                        return m_Instance;
                    }
                    else
                    {
                        GameObject go = new GameObject();
                        go.name = "Uploader";
                        m_Instance = go.AddComponent<BundleUploader>();
                        return m_Instance;
                    }
                }
                else
                {
                    return m_Instance;
                }
            }
        }

        #region static methods
        public static void UpdateBundle(string bundlePath, string userId, int ieId, string bundleVersion, string buildTarget)
        {
            Instance.StartCoroutine(Instance.OnUpload(bundlePath, userId, ieId, bundleVersion, buildTarget));
        }
        #endregion

        IEnumerator OnUpload(string bundlePath, string userId, int ieId, string bundleVersion, string buildTarget)
        {
            IsUploading = true;

            if (File.Exists(bundlePath))
            {
                Debug.Log("Upload file : " + bundlePath);

                FileStream fs = new FileStream(bundlePath, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] data = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();

                WWWForm form = new WWWForm();
                form.AddField("userId", userId);
                if (ieId != 0)
                {
                    form.AddField("ieId", ieId.ToString());
                }
                if (bundleVersion != "None")
                {
                    form.AddField("bundleVer", bundleVersion.ToString());
                }
                if (buildTarget != "None")
                {
                    form.AddField("updateType", buildTarget.ToString());
                }
                form.AddField("res", "");
                form.AddBinaryData("res", data, System.IO.Path.GetFileName(bundlePath), null);

                using (WWW www = new WWW("http://console.sightp.com/service/SightPlusToolApi/UpdateAssetBundleResourceAIeId", form))
                {
                    //yield return www;
                    while (!www.isDone)
                    {
                        UploadProgress = www.uploadProgress;

                        yield return 0;
                    }

                    if (www.error == null)
                    {
                        Debug.Log("Upload result :\n" + www.text);
                    }
                    else
                    {
                        Debug.LogError(www.error);
                    }
                }
            }

            IsUploading = false;

            yield return 0;
        }
    }
}
